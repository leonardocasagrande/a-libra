<?php

global $header_class, $header_title;

$header_class = "banner-search";

$header_title = "Pesquisa";

?> <?php get_header(); ?> <div class="container mt-5 py-4"><div class="row mb-5"> <?php include('form-search.php') ?> </div><div class="row posts-blog py-5 mb-5"> <?php

    $s = get_search_query();

    $args = array(
        's' =>$s
    );
    
    $the_query = new WP_Query( $args );

    if ( $the_query->have_posts() ) {

        while ( $the_query->have_posts() ) {

            $the_query->the_post();

        ?> <a href="<?php the_permalink() ?>" title=""><div class="col-md-6"><div class="col-12"><div class="date pl-5 pt-3"> <?= get_the_date('d/m/Y'); ?> </div><div class="title px-5"> <?php the_title(); ?> </div><div class="description py-2 px-5"> <?= substr_replace(get_the_content(), "...", 130);?> </div><a class="read-more px-5" href="<?php the_permalink() ?>" title="<?php the_title(); ?>">Continuar Lendo <i class="fas fa-arrow-right"></i></a></div></div></a> <?php
        }
    }else{ ?><div class="text-center m-auto"><h2 style="font-weight:bold;color:#000">Não encontrei nada</h2><div class="col-12"><p>Desculpe, mas nada corresponde aos seus critérios de busca.<br>Por favor, tente novamente com algumas palavras-chave diferentes.</p></div></div> <?php } ?> </div></div> <?php get_footer(); ?>