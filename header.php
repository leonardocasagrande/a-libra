<!DOCTYPE html><html lang="pt_BR"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width,initial-scale=1"><title> <?php wp_title(''); ?> </title><meta name="adopt-website-id" content="0d1f91ef-3ad5-4d11-b08e-ac8abe9b794c"><script src="//tag.goadopt.io/injector.js?website_code=0d1f91ef-3ad5-4d11-b08e-ac8abe9b794c" class="adopt-injector"></script><meta name="robots" content="index, follow"><meta name="msapplication-TileColor" content="#ffffff"><meta name="theme-color" content="#ffffff"> <?php wp_head(); ?> <style>/* NO SELECT + HIGHLIGHT COLOR */
    * {
      user-select: none;
    }

    *::selection {
      background: none;
    }

    *::-moz-selection {
      background: none;
    }</style><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-158718183-1"></script><script>window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-158718183-1');</script></head><body><noscript id="deferred-styles"></noscript><link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css"><header><div style="height: 109px" class="height-breaf d-none"></div><div class="mini-bar d-none d-lg-block bg-light"><div class="container"><div class="row align-items-center justify-content-around col-md-9 m-auto hover08"><a href="<?= get_site_url(); ?>/produtos/#food-service"><figure><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-alibralac.png" alt="ALibraLAC - Compostos lácteos em pó e pó para o preparo de alimentos e bebidas" title="ALibraLAC - Compostos lácteos em pó e pó para o preparo de alimentos e bebidas"></figure></a><a href="<?= get_site_url(); ?>/produtos/#sorvetes-artesanais"><figure><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-multmix.png" alt="Multmix" title="Multmix"></figure></a><a href="<?= get_site_url(); ?>/produtos/#food-service"><figure><img src=" <?= get_stylesheet_directory_uri(); ?>/dist/img/logo-mozzana.png" alt="Mozzana - Indústria de cobertura para pizza" title="Mozzana - Indústria de cobertura para pizza"></figure></a><a href="<?= get_site_url(); ?>/produtos/#varejo"><figure><img style="width: 90px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-merilu1.png" alt="Merilú - Composto Lácteo Merilú" title="Merilú - Composto Lácteo Merilú"></figure></a><a href="<?= get_site_url(); ?>/produtos/#varejo"><figure><img style="width: 90px;" src=" <?= get_stylesheet_directory_uri(); ?>/dist/img/logo-merilu.png" alt="Merilú - Composto Lácteo Merilú" title="Merilú - Composto Lácteo Merilú"></figure></a><a href="<?= get_site_url(); ?>/produtos/#genkorlac"><figure><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/GenkorLac.png" alt="GenkorLac - Ingredientes para produtos lácteos" title="GenkorLac - Ingredientes para produtos lácteos"></figure></a><!-- <div id="google_translate_element"></div> --></div></div></div><div class="bg-white nav-scroll"><div class="container"><nav class="d-flex justify-content-between py-md-3 flex-wrap navbar-expand-lg bg-white"><a class="navbar-brand pl-5 pl-lg-0" href="/"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-alibra.png" alt="Alibra Ingredientes" title="Alibra Ingredientes"> </a><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars text-black"></i></button><div class="collapse navbar-collapse py-3" id="navbarSupportedContent"><div class="menuOver"><div class="container"><div class="menuOverDesk"><div class="menuOver-item"><div id="google_translate_element"></div></div><div class="menuOver-item"><a href="<?= get_site_url(); ?>/carreiras/" class="nav-link">CARREIRAS</a> <a href="<?= get_site_url(); ?>/politica-de-privacidade/" class="nav-link">POLÍTICA DE PRIVACIDADE</a></div></div></div></div><ul class="navbar-nav align-items-center m-auto"><li class="nav-item active"><a class="nav-link" href="<?= get_site_url(); ?>/quem-somos/">Quem Somos</a></li><li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/produtos/">Produtos</a></li><li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/esg/">ESG</a></li><li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/compliance/">Compliance</a></li><li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/blog/">Blog</a></li><li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/pesquisa/"><i class="fas fa-search"></i></a></li><li><a class="nav-link bg-green rounded-pill text-white px-5 py-3" href="<?= get_site_url(); ?>/fale-conosco/" class="bg-green text-uppercase">Fale Conosco</a></li><li class="py-2 py-lg-0"><a target="_blank" class="px-2 color-blue" href="https://www.facebook.com/alibraingredientes"><i class="fab fa-facebook-f"></i> </a><a target="_blank" class="px-2 color-yellow" href="https://www.instagram.com/alibraingredientes/"><i class="fab fa-instagram"></i> </a><a target="_blank" class="px-2 color-skyblue" href="https://www.linkedin.com/company/alibra-ingredientes-sa/"><i class="fab fa-linkedin-in"></i> </a><a target="_blank" class="px-2 color-red" href="https://www.youtube.com/channel/UCYgMHpjXgzpRilH6sCIvhkQ/videos"><i class="fab fa-youtube"></i> </a><span class="px-2" id="myBtn"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/btn-modal.png" alt="btnModal"></span></li></ul></div></nav></div><div class="buton-floats"><a class="float-button" href="tel:+551937168888"><div class="text-white d-nonee fone mr-2 pl-2">(19) 3716-8888</div><i class="fas fa-phone my-float"></i></a></div></div></header> <?php

  global $header_class;

  $is_single = is_single();

  $is_home = is_page('home');

  if ($is_single == false and $is_home == false) {

    if (is_null($header_class)) {

      global $post;

      $header_class =  'banner-'  . $post->post_name;
    }

  ?> <section class="banner <?= $header_class ?> text-white"> <?php include('header-breadcrumbs.php') ?> </section> <?php } ?></body></html>