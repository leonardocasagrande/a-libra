<footer class="bg-dblue"><div class="container py-3"><div class=""><div class="d-md-flex justify-content-md-between text-center text-md-left pt-3"><img class="filter-white pr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-alibra.png" alt="Alibra Ingredientes" title="Alibra Ingredientes"><ul class="d-md-flex flex-wrap list align-items-center p-0"><li class="nav-item active"><a class="nav-link text-white" href="<?= get_site_url(); ?>/quem-somos/">Quem Somos</a></li><li class="nav-item"><a class="nav-link text-white" href="<?= get_site_url(); ?>/produtos/">Produtos</a></li><li class="nav-item"><a class="nav-link text-white" href="<?= get_site_url(); ?>/esg/">ESG</a></li><li class="nav-item"><a class="nav-link text-white" href="<?= get_site_url(); ?>/compliance/">Compliance</a></li><li class="nav-item"><a class="nav-link text-white" href="<?= get_site_url(); ?>/blog/">Blog</a></li></ul></div></div><div class="pb-3 pt-md-4"><div class="row align-items-center"><div class="col-md-6 align-items-center text-white pr-md-0"><p class="mt-2"><strong class="color-green">Matriz</strong><br>Parque Tecnológico Techno Park<br>Av. Alexander Graham Bell, 200, Unidade: D1<br>CEP 13069-310 | Techno Park | Campinas - SP<br></p><p></p><p><strong class="color-green">Telefone Comercial:</strong> +55 (19) 3716-8888</p></div><div class="col-md-6 align-items-center text-white mt-4"><p><strong class="color-green">Filial</strong><br>Rodovia BR 163, s/n Km 283,6 - Pq. Industrial<br>CEP 85960-000 | Marechal Cândido Rondon - PR</p></div><div class="container"><hr class="bg-white"></div><div class="container py-3"><div class="d-md-flex text-center"><div class="col-md-6 pl-0 text-white text-md-left text-center">©<?= date('Y') ?> Alibra S.A - Todos os direitos reservados.</div><div class="col-md-6 mt-4 mt-md-0 pr-0"><div class="d-flex justify-content-lg-end justify-content-center"></div></div></div></div></div></div></div></footer><!-- <span class="btn-modalwww" >
 
</span> --><style>/* The Modal (background) */
.modalwww {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 999; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modalwww Content */
.modalwww-content {
  position:relative;
  margin: auto;
  width: 100%;
  text-align:center;
  max-width:920px;
}

/* The Close Button */
.close {
  display:block;
  position:absolute;
  top: 65px;
  right: 50px;
  width: 45px;
  height: 45px;
}
@media (min-width:1024px) {
  .close{
    top:  12px;
    right: 70px;
  }
}
.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}</style><!-- The Modalwww --><div id="myModalwww" class="modalwww"><!-- Modalwww content --><div class="modalwww-content"><span class="close"></span> <a href="https://drive.google.com/file/d/1b5zgjxvTeD_fxMCVjnTEs7De13crNh3z/view" target="_blank" rel="noopener noreferrer"><img class="d-lg-none img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/11671_pop-up_site_mobile_350x350.png" alt=""> <img class="d-none d-lg-block img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/11671_pop-up_site_desktop_1140x500.png" alt=""></a></div></div> <?php wp_footer(); ?> <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script><script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script><!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/main.js"></script> --><script>// PREVENT CONTEXT MENU FROM OPENING
  document.addEventListener("contextmenu", function(evt) {
    evt.preventDefault();
  }, false);

  // PREVENT CLIPBOARD COPYING
  document.addEventListener("copy", function(evt) {
    // Change the copied text if you want
    evt.clipboardData.setData("text/plain", "Copying is not allowed on this webpage");

    // Prevent the default copy action
    evt.preventDefault();
  }, false);</script><!-- <script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script>
      function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'pt-BR',
            includedLanguages: 'en,es'
          },
          'google_translate_element'
        );
      }
</script> --><script>// Get the modalwww
var modalwww = document.getElementById("myModalwww");

// Get the button that opens the modalwww
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modalwww
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modalwww 
// function domReady () {
//   modalwww.style.display = "block";
// }
btn.onclick = function() {
  modalwww.style.display = "block";
}
// if ( document.addEventListener ) {
//   document.addEventListener( "DOMContentLoaded", function(){
//     document.removeEventListener( "DOMContentLoaded", arguments.callee, false);
//     domReady();
//   }, false );

// // If IE event model is used
// } else if ( document.attachEvent ) {
//   // ensure firing before onload
//   document.attachEvent("onreadystatechange", function(){
//     if ( document.readyState === "complete" ) {
//       document.detachEvent( "onreadystatechange", arguments.callee );
//       domReady();
//     }
//   });
// }

// When the user clicks on <span> (x), close the modalwww
span.onclick = function() {
  modalwww.style.display = "none";
}

// When the user clicks anywhere outside of the modalwww, close it
window.onclick = function(event) {
  if (event.target == modalwww) {
    modalwww.style.display = "none";
  }
}</script>