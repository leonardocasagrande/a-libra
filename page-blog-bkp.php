<?php get_header(); ?> <div class="container mt-5 pt-4"> <?php
  $categories = get_categories(array(
    'orderby' => 'name',
    'order'   => 'ASC'
  ));
  ?> <div class="row"><div class="ftsz-26 color-dgrey mt-n5">Categorias:</div><div class="blog-nav-menu" id="tile-1"><ul class="nav nav-tabs nav-justified" role="tablist"><div class="slider"></div><li class="nav-item"><a class="nav-link active" id="nav-blog-all" data-toggle="tab" href="#tab-blog-all" role="tab" aria-controls="all" aria-selected="true">Todos</a></li> <?php foreach ($categories as $category) { ?> <li class="nav-item"><a class="nav-link" id="nav-blog-<?= $category->term_id ?>" data-toggle="tab" href="#tab-blog-<?= $category->term_id ?>" role="tab" aria-controls="tab-blog-<?= $category->term_id ?>" aria-selected="false"> <?= $category->name ?></a></li> <?php } ?> </ul><div class="tab-content pt-4 pb-5"><div class="tab-pane fade show active" id="tab-blog-all" role="tabpanel" aria-labelledby="nav-blog-all"><div class="row posts-blog"> <?php $catquery = new WP_Query("cat=0&posts_per_page=-1"); ?> <?php while ($catquery->have_posts()) : $catquery->the_post(); ?> <div class="col-md-6 pb-3"><div class="col-12"><a href="<?php the_permalink() ?>" title=""> <?php

                    $thumbnail = get_the_post_thumbnail_url();

                    if (strlen($thumbnail) == 0) {

                      $thumbnail = "https://via.placeholder.com/1024x380";
                    } ?> <img src="<?= $thumbnail ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"></a></div><div class="col-12"><div class="date pl-5 pt-3"> <?= get_the_date('d/m/Y'); ?> </div><div class="title px-5"> <?php the_title(); ?> </div><div class="description py-2 px-5"> <?= get_the_excerpt(); ?> </div><a class="read-more px-5" href="<?php the_permalink() ?>" title="<?php the_title(); ?>">Continuar Lendo <i class="fas fa-arrow-right"></i></a></div></div> <?php endwhile;
            wp_reset_postdata();
            ?> </div></div> <?php foreach ($categories as $category) { ?> <div class="tab-pane fade" id="tab-blog-<?= $category->term_id ?>" role="tabpanel" aria-labelledby="nav-blog-<?= $category->term_id ?>"><div class="row posts-blog"> <?php $catquery = new WP_Query("cat=$category->term_id&posts_per_page=-1");

              while ($catquery->have_posts()) : $catquery->the_post();
              ?> <div class="col-md-6"><div class="col-12"><a href="<?php the_permalink() ?>" title=""> <?php

                      $thumbnail = get_the_post_thumbnail_url();

                      if (strlen($thumbnail) == 0) {

                        $thumbnail = "https://via.placeholder.com/1024x380";
                      } ?> <img src="<?= $thumbnail ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"></a></div><div class="col-12"><div class="date pl-5 pt-3"> <?= get_the_date('d/m/Y'); ?> </div><div class="title px-5"> <?php the_title(); ?> </div><div class="description py-2 px-5"> <?= get_the_excerpt(); ?> </div><a class="read-more px-5" href="<?php the_permalink() ?>" title="<?php the_title(); ?>">Continuar Lendo <i class="fas fa-arrow-right"></i></a></div></div> <?php endwhile;
              wp_reset_postdata();
              ?> </div></div> <?php } ?> </div></div></div></div> <?php get_footer(); ?>