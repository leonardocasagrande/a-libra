<?php get_header('blog'); ?>

<div class="container text-center mt-5 blog-single">

  <h2 class="col-md-9 color-blue mt-n5 m-auto">
    <?php the_title(); ?>
  </h2>

  <div class="col-12">
    <?= get_the_date('d/m/Y'); ?>
  </div>

  <div class="col-12 my-5 featured-imagen">
    <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
  </div>
  <div class="position-relative">
    <div class="social-links-blog">
      <a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink(); ?>"><i class="fab fa-facebook-f"></i></a>
      <a href="https://twitter.com/share?url=<?= get_permalink(); ?>"><i class="fab fa-twitter"></i></a>
      <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= get_permalink(); ?>"><i class="fab fa-linkedin-in"></i></a>
    </div>
  </div>
  <div class="col-md-10 m-auto text-left">
    <?php the_content() ?>
  </div>

  <div class="col-md-10 m-auto pb-5">
    <div class="fb-comments" data-href="<?= the_permalink(); ?>" data-width="100%" data-numposts="5"></div>
  </div>

</div>

<?php get_footer(); ?>