<?php


global $header_title;

$header_title = "";

get_header();

?>


<div class="pb-5">

    <div class="container text-center mt-5 ">

        <img class="ml-md-5 ml-3 mb-n2 mb-lg-4 mb-n" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/produtos_alibra.png" alt="Produtos A Libra" title="Produtos Alibra">

        <div class="ftsz-26 color-dgrey mt-md-n4">Selecione abaixo a linha que deseja:</div>

    </div>

    <div class="position-relative">

        <div class="d-flex justify-content-center mt-4 py-md-1">

            <div class="flex-initial overflow-x nav container nav-cat justify-content-md-start justify-content-lg-center" role="tablist">


                <?php

                $categories = get_terms(
                    array(
                        'parent' => 0,
                        'taxonomy' => 'categoria'
                    )
                );

                foreach ($categories as $cat) { ?>

                    <a class="px-2 symbolcat" data-toggle="tab" href="#<?= $cat->slug; ?>" role="tab" id="link-<?= $cat->slug; ?>">


                    </a>

                <?php } ?>



            </div>

        </div>

    </div>

    <div class="tab-content container">

        <?php foreach ($categories as $cat) { ?>

            <div role="tabpanel" class="tab-pane text-center" id="<?= $cat->slug; ?>">

                <p class="color-dgrey my-md-4 my-4 text-center d-none d-md-block">

                    <?= $cat->description ?>

                </p>


                <div class="help d-lg-none text-center my-2">



                    <i class="fas fa-arrows-alt-h"></i> <br>

                    Arraste para o lado para ver todas as categorias

                </div>

                <?php if ($cat->slug != "industria-de-alimentos-e-de-bebida") { ?>

                    <div class="d-flex flex-wrap">

                        <?php

                        $paged = get_query_var('paged') ? get_query_var('paged') : 1;


                        $args = array(

                            'post_type' => 'produtos',

                            'order' => 'ASC',

                            'posts_per_page' => '-1',

                            'paged' => $paged,

                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'categoria',
                                    'field'    => 'slug',
                                    'terms'    =>  $cat->slug,
                                )
                            )
                        );

                        $loop = new wp_query($args);

                        while ($loop->have_posts()) : $loop->the_post() ?>

                            <?php

                            while (have_rows('produtos')) : the_row();
                                $sub_value = get_field('foto_externa');
                                $sub_description = get_field('descricao_da_linha');

                            endwhile;


                            $post_slug = get_post_field('post_name', get_post());

                            $link = $cat->slug . '/' . $post_slug;

                            ?>

                            <div class="col-md-3 text-center my-3 rounded">

                                <a href="<?= $link ?>">

                                    <div class="border-g shadow">

                                        <img src=" <?= $sub_value['url'] ?>" style="max-height: 223px; height: 100%;">

                                        <div class="w-100 bg-light box-products color-blue p-3">

                                            <div class="box">

                                                <h5 class="my-3 text-uppercase font-weight-bold"> <?= the_title() ?> </h5>

                                                <h5 class="color-green"> <?= $sub_description ?> </h5>

                                            </div>

                                        </div>

                                    </div>

                                </a>

                            </div>

                        <?php endwhile;  ?>

                    </div>

                <?php  } else { ?>

                    <div>

                        <div class="d-flex overflow-x cat-child sub-cat-header-<?= $cat->slug; ?>"></div>



                        <div class="d-flex cat-child overflow-x">

                            <?php

                            $subcategories = get_term_children($cat->term_id, 'categoria');

                            foreach ($subcategories as $sub) {

                                $term = get_term_by('id', $sub, 'categoria');

                                $args = array(
                                    'public'   => true,
                                    '_builtin' => false
                                );

                            ?>



                                <a href="#<?= $term->slug ?>" id="<?= $term->slug ?>" data-setcat="<?= $term->slug ?>" data-category-father="<?= $cat->slug; ?>" class="col-6 col-lg-3 set-cat-<?= $cat->slug; ?> set-cat m-auto">

                                    <?php
                                    $category_image = get_field('imagem_categoria',  $term->taxonomy . '_' . $term->term_id);


                                    ?>
                                    <img src="<?= $category_image['url'] ?>"> <br>

                                    <div class="bg-light desc flex-wrap">

                                        <div class="box">

                                            <div class="name-cat mb-2">

                                                <?= $term->name; ?>

                                            </div>

                                            <div class="desc-cat d-none d-md-block">

                                                <?= $term->description; ?>

                                            </div>


                                        </div>

                                </a>

                                <div class="get-cat w-100 d-none" data-category="<?= $term->slug ?>" id="get-cat-<?= $term->slug ?>">


                                    <?php
                                    $long_desc = get_field('descricao_longa',  $term->taxonomy . '_' . $term->term_id);


                                    ?>

                                    <p class="font-weight-bold mt-4"><?= $long_desc ?></p>


                                    <div class="col-12 my-4">

                                        <hr>

                                    </div>

                                    <div class="d-flex flex-wrap">

                                        <?php

                                        $paged = get_query_var('paged') ? get_query_var('paged') : 1;


                                        $args = array(

                                            'post_type' => 'produtos',

                                            'order' => 'ASC',

                                            'posts_per_page' => '-1',

                                            'paged' => $paged,

                                            'tax_query' => array(
                                                array(
                                                    'taxonomy' => 'categoria',
                                                    'field'    => 'slug',
                                                    'terms'    =>   $term->slug,
                                                )
                                            )
                                        );

                                        $loop = new wp_query($args);

                                        while ($loop->have_posts()) : $loop->the_post() ?>

                                            <?php

                                            while (have_rows('produtos')) : the_row();
                                                $sub_value = get_field('foto_externa');
                                                $sub_description = get_field('descricao_da_linha');

                                            endwhile;

                                            ?>


                                            <div class="col-lg-3 col-md-6 text-center my-3 rounded">

                                                <a href="<?= get_permalink(); ?>">

                                                    <div class="border-g shadow">

                                                        <img src=" <?= $sub_value['url'] ?>" style="max-height: 223px">

                                                        <div class="w-100 bg-light box-products color-blue p-3">

                                                            <div class="box">

                                                                <h5 class="my-3 text-uppercase font-weight-bold"> <?= the_title() ?> </h5>

                                                                <h5 class="color-green"> <?= $sub_description ?> </h5>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>


                                        <?php endwhile;

                                        //VAREJO


                                        ?>

                                    </div>


                                </div>

                            <?php } ?>

                        </div>

                    </div>
                <?php } ?>
            </div>

        <?php }  ?>

    </div>

</div>




<?php get_footer(); ?>

<script>
    $(document).ready(function() {
        // Javascript to enable link to tab    
        var hash = document.location.hash;

        $(hash).trigger("click");
        console.log(hash);
    });
</script>