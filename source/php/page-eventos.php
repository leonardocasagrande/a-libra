<?php get_header(); ?>

<section>
  <div class="container mt-5 pt-4">
    <div class="col-md-10 m-auto">
      <p class="text-center paragraph-event">A Alibra participa dos principais e mais significativos eventos e rodadas de negócio do setor no Brasil e no exterior. Nestes eventos é possível conhecer a extensa linha de ingredientes e produtos finais para os diversos segmentos em que atua, degustar produtos desenvolvidos com soluções inovadoras e fazer contato com a nossa equipe comercial e técnica.
        <br>

      </p>
    </div>
  </div>
</section>

<section class="section-videos my-5">
  <div class="container">


    <div class="row align-items-center py-5">


      <div class="col-md-8">



        <iframe class="format-iframe" width="560" height="315" src="https://www.youtube.com/embed/GApEsc9YBh4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


      </div>

      <div class="col-md-4 d-flex justify-content-center">
        <h2 class="title-fispal text-md-left text-center py-4">Fispal <br> 2019</h2>
      </div>

    </div>

    <!-- <div class="row align-items-center py-5">
      <div class="col-md-4 d-flex justify-content-center">
        <h2 class="title-fispal text-md-left text-center py-4">Fipan <br> 2019</h2>
      </div>

      <div class="col-md-8">
        <iframe class="format-iframe" src="https://www.youtube.com/embed/g-IESVguILA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
        </iframe>
      </div>
    </div> -->

    <hr class="hr-color">

    <div class="row align-items-center py-5">
      <div class="col-md-4 d-flex justify-content-center">
        <h2 class="title-fispal text-md-left text-center py-4">FISA <br> 2018</h2>

      </div>
      <div class="col-md-8">
        <iframe class="format-iframe" src="https://www.youtube.com/embed/E_PIn44RC1I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>

    </div>

  </div>
</section>



<?php get_footer(); ?>