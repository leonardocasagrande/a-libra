<?php get_header(); ?>

<section class="compliance-start">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 text-center text-lg-left">
                <h2 class="titulo color-blue">Uma empresa, assim como as pessoas, possui personalidade</h2>
                <p>A Alibra em sua cultura organizacional é formada pelas essências: Adaptabilidade, Excelência e Visão de Cliente, Eficiência, Resultados Sustentáveis, Cooperação, e Valorização de Pessoas.</p>
                <p>Para garantir que essa jornada de sucesso se perpetue e esteja alinhada aos movimentos da sociedade, aos diferentes perfis de profissionais e ao seu crescimento orgânico, foi desenvolvido o Código de Conduta e Ética Alibra.</p>
            </div>
            <div class="col-lg-5">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/compliance.png" alt="">
            </div>
        </div>
    </div>
</section>
<section class="presidente">
    <img class="efeito-azul" src="<?= get_stylesheet_directory_uri() ?>/dist/img/efeito-azul.png" alt="">

    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h2 class="color-green">Mensagem <br>do Presidente</h2>
            </div>
            <div class="col-lg-6">
                <img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/presidente.png" alt="">
                <img class="efeito-roxo" src="<?= get_stylesheet_directory_uri() ?>/dist/img/efeito-roxo.png" alt="">
            </div>
        </div>
        <div class="row align-items-start">
            <div class="col-12">
            <h3 class="color-green">Caros colegas de jornada,</h3>

            </div>
            <div class="col-lg-6">
                A valorização das pessoas é, com certeza, o alicerce do nosso crescimento e está pautado na ética, na transparência e na seriedade em todos os processos. Por isso, em toda a nossa trajetória desenvolvemos soluções convenientes e nutritivas que alimentam o mercado consumidor, sempre com foco em resultados sustentáveis e respeito em todas as relações: indivíduos, comunidade e meio ambiente. 
                <br><br>
                Ano a ano evoluímos e, sem nos perdermos de nossa essência e dos valores que cultivamos, nos transformamos em uma companhia que vem se destacando cada vez mais no desenvolvimento de produtos alimentícios, por meio de soluções aderentes às necessidades do mercado e que agregam valor a clientes e consumidores.
                <br><br>
                Nesta busca diária e incansável para que nossos produtos façam a diferença na vida das pessoas, somos hoje uma empresa renomada no setor em que atuamos. Sem dúvida, uma grande satisfação fazer parte de uma organização que preza por produtos alimentícios inovadores, seguros e saborosos, que chegam à mesa de milhares de lares no Brasil e no exterior. 
                <br><br>
                Crescer, no entanto, é também nos comprometermos com a integridade e o desenvolvimento sustentável, e, para garantir que essa trajetória de sucesso se perpetue e esteja
            </div>
            <div class="col-lg-6">
                alinhada aos movimentos da sociedade e aos diferentes perfis de profissionais, apresentamos o Código de Conduta e Ética do Colaborador ALIBRA.
                <br><br>
                Este Código é um importante guia na condução dos nossos negócios, uma vez que o sucesso da ALIBRA depende de cada uma de nossas escolhas e decisões. Assim, contamos com você, protagonista da nossa história, para ajudar a zelar, incentivar e difundir a necessidade de cumprimento das diretrizes éticas aqui abordadas.
                <br><br>
                Sempre que necessário consulte este Código e, em caso de dúvida ou de um dilema ético, não hesite em buscar esclarecimento junto ao Canal de Ética ou à gerência de Recursos Humanos. Juntos diferenciaremos a ALIBRA não apenas como referência no setor de alimentos, mas como líder em termos de ética.
                <br><br>
                Boas práticas de gestão empresarial e confiança na equipe e no relacionamento duradouro com clientes, sociedade e demais públicos com os quais nos vinculamos é o que nos move.
                
                <h3><span class="color-green">Vamos juntos?</span><br>Alibra, juntos, criamos inovações que alimentam!</h3>
            </div>
            <div class="col-12 col-lg-8 m-auto mt-lg-4">
            <a target="_blank" href="<?= get_site_url() ?>/pdfs/Alibra_Codigo de conduta_V1.pdf" class="btn-cta d-lg-none"> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/seta-download.png" alt=""> <br>tenha acesso à cartilha completa de Compliance</a>
            <a target="_blank" href="<?= get_site_url() ?>/pdfs/Alibra_Codigo de conduta_V1.pdf" class="btn-cta d-none d-lg-flex"> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/seta-download.png" alt=""> tenha acesso à cartilha completa de Compliance</a>

            </div>
        </div>
    </div>
    <img class="efeito-laranja" src="<?= get_stylesheet_directory_uri() ?>/dist/img/efeito-laranja.png" alt="">
    <img class="efeito-verde-compliance d-none d-lg-block" src="<?= get_stylesheet_directory_uri() ?>/dist/img/efeito-verde-compliance.png" alt="">

</section>
<?php get_footer(); ?>