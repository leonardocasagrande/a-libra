<?php

global $header_class, $header_title;

$header_class = "banner-search";

$header_title = "Pesquisa";

?>

<?php get_header(); ?>

<div class="container mt-5 py-4">


    <div class="row mb-5">
        
        <?php include('form-search.php') ?>

    </div>

</div>
<?php get_footer(); ?>