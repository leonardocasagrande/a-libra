<!DOCTYPE html>

<html lang="pt_BR">

<head>

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>

    <?php wp_title(''); ?>

  </title>
  <meta name="adopt-website-id" content="0d1f91ef-3ad5-4d11-b08e-ac8abe9b794c" />
  <script src="//tag.goadopt.io/injector.js?website_code=0d1f91ef-3ad5-4d11-b08e-ac8abe9b794c" class="adopt-injector"></script>
  
  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>

</head>

<body>

  <noscript id="deferred-styles"></noscript>

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">

  </noscript>

  <header>

    <div class="container py-2">

      <nav class="d-flex justify-content-between my-md-3 flex-wrap navbar-expand-lg">

        <a class="navbar-brand pl-4" href="/">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-alibra.png" alt="Alibra Ingredientes" title="Alibra Ingredientes">

        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fas fa-bars text-black"></i>
        </button>

        <div class="collapse navbar-collapse  py-3 justify-content-end" id="navbarSupportedContent">

          <ul class="navbar-nav align-items-center">

            <li class="nav-item active">

              <a class="nav-link color-green" target="_blank" href="http://site.vagas.com.br/alibra?url=http://www.vagas.com.br/alibra&t=v">Oportunidades</a>

            </li>


            <li class="nav-item active">

              <a class="nav-link color-green" target="_blank" href="https://site.vagas.com.br/IdCandColetaCur.asp">Deixe seu currículo</a>

            </li>

            <li class="nav-item active">

              <a class="nav-link color-green" target="_blank" href="http://site.vagas.com.br/alibra?url=http://www.vagas.com.br/alibra&t=ac">Área do canditado</a>

            </li>


          </ul>

        </div>

      </nav>

    </div>

  </header>