<?php get_header(); ?>




<section class="section-title">

  <div class="container default-spacing">
    <div class="row align-items-center">
      <div class="col-md-12 text-md-left text-center">
        <h2 class="title-about px-3 px-md-0 class-spacing text-uppercase">Lei Geral de Proteção de Dados</h2>
        <p class="paragraph-format px-3 px-md-0">
        A LGPD - Lei Geral de Proteção de Dados - regulamenta o tratamento de dados pessoais, com objetivo de proteger a liberdade
e a privacidade dos titulares.<br>
A Alibra Ingredientes preza pela privacidade de seus titulares e cada vez mais vem aperfeiçoando seus processos
em conformidade com a LGPD.<br>
        </p>
      </div>

    </div>
  </div>
</section>

<section class=" section-bia bg-blue mb-md-5 ">
  <img class="d-none d-lg-block right-0  position-absolute efeito-bia"  src="<?= get_stylesheet_directory_uri(); ?>/dist/img/azul.webp">
  <div class="container-fluid pl-md-0">
    <div class="row flex-row-reverse">
      
      <div class="col-md-7">
        <p class="paragraph paragraph-bia py-3 col-md-8 ml-md-5 text-md-left text-center text-white">
          Confira a <span class="color-green">Política de Privacidade</span> <br>da Alibra.<br><br>
          <a href="https://www.calameo.com/read/0070241923588e225cddd" target="_blank" class="btn-cta-bia">Acessar</a>      
        </p>  
      </div>
      <div class="col-md-5 px-0 mt-sm-6 text-md-left text-center">
        <img class="img-fluid img-bia d-md-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/bia_mascote_sem_slogan.png">
      </div>
      <img class="img-fluid img-bia d-none d-md-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/bia_mascote_sem_slogan.png">

    </div>
  </div>
</section>
<section class="py-3"></section>
<?php get_footer(); ?>