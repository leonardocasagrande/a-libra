<?php

get_header();

$categories = get_the_terms($post->ID, 'categoria');


// var_dump($categories);
?>
<?php $link_receita = get_field('url_das_receitas'); ?>
<div class="single-produtos">

    <div class="container my-3 pb-4">

    <ul class="breadcrumbs">
        <li class="bread"><a href="<?= get_site_url() ?>">Início &nbsp;> </a></li>
        <li class="bread"><a href="<?= get_site_url() ?>/produtos">Produtos &nbsp;> </a></li>
        <?php foreach ($categories as $categorie) { ?>
            <?php if($categorie->parent == 17){

            }else{ ?>
            <li class="bread"><a href="<?= get_site_url() ?>/produtos/#<?= $categorie->slug ?>"><?= $categorie->name ?> &nbsp;> </a></li>

            <?php } ?>
        <?php } ?>
        <li class="bread"><?php the_title() ?></li>
    </ul>
        <!-- <a href="<?= get_site_url() . '/produtos' . '#' . $categoria ?> " class="bg-blue text-white rounded-pill p-2 px-4 font-weight-bold">

            <i class="fas fa-arrow-left mr-1"></i>

            Voltar

        </a> -->

    </div>

    <section class="banner text-white">



        <?php // include('header-breadcrumbs.php') 
        ?>



    </section>

    <?php

    $tipo_do_produto = get_field("tipo_do_produto");

    // // check if the repeater field has rows of data
    // if (have_rows('produtos')) :

    //     // loop through the rows of data
    //     while (have_rows('produtos')) : the_row();

    //         // display a sub field value
    //         $nome_do_produto = get_sub_field('nome_do_produto');
    //         $descricao = get_sub_field('descricao'); //
    //         $sub_descricao = get_sub_field('sub_descricao');
    //         $imagem_do_produto = get_sub_field('imagem_do_produto');

    //     endwhile;

    // else :

    // // no rows found

    // endif;

    ?>
    <div class="container">

        <div class="row" id="accordionDescricao">
            <?php
            if (have_rows('produtos')) : $count = 1; ?>

                <?php while (have_rows('produtos')) : the_row();


                    $logo = get_sub_field('logo');

                    $nome_do_produto = get_sub_field('nome_do_produto');
                    $descricao = get_sub_field('descricao'); //
                    $sub_descricao = get_sub_field('sub_descricao');
                    $imagem_do_produto = get_sub_field('imagem_do_produto');
                    $selo = get_sub_field('selo');
                    $gramatura_litro = get_sub_field('gramatura_litro');
                    $diferenciais = get_sub_field('diferenciais');


                ?>


                    <!-- -->

                    <div class="col-md-6">
                        <div class=" px-5 position-relative">
                            <?php if ($selo) { ?>
                                <img style="max-width: 115px; position: absolute; top: -16px; left: -30px;" src="<?= $selo ?>">

                            <?php } ?>



                        </div>
                        <img class="alignleft" src="<?= $imagem_do_produto ?>" alt="" style="max-width:40%;">
                        <div class="d-md-flex align-items-center flex-wrap justify-content-between botao-do-acordeao collapsed" id="heading-<?= $count; ?>" data-toggle="collapse" data-target="#collapse-<?= $count; ?>" aria-expanded="true" aria-controls="collapse-<?= $count; ?>">

                            <div class="title <?php if ($logo) : ?> col-md-7 <?php else : echo "col-md-12";
                                                                            endif; ?> " style="line-height: 3px;padding-left: 0 !important;">
                                <h3> <?= $nome_do_produto ?></h3>

                                <small class="font-weight-bold" style="font-size: 14px;" class="color-blue">




                                    <?php

                                    foreach ($gramatura_litro as $value) {

                                        echo  $value['quantidade'] . $value['unidade_de_medida'];




                                    ?>



                                    <?php } ?>

                                </small>

                            </div>
                            <?php if ($logo) : ?>
                                <div class="logo col-md-5" style="max-width: 200px">

                                    <img src="<?= $logo['link'] ?>" alt="">

                                </div>
                            <?php endif; ?>
                            <div class="sub-title col-12">
                                <?= $sub_descricao ?>
                            </div>
                            Leia Mais
                        </div>


                        <div class="description collapse" aria-labelledby="heading-<?= $count; ?>" data-parent="#accordionDescricao" id="collapse-<?= $count; ?>">
                            <?php if ($selo) { ?>
                                <img class="alignright d-lg-none" style="max-width: 115px; position: absolute; top: -16px; left: -30px;" src="<?= $selo ?>">

                            <?php } ?>
                            <?= $descricao ?>

                            <?php if ($diferenciais) { ?>
                                <h3 class="color-red">Diferenciais</h3>

                                <?= $diferenciais; ?>
                            <?php } ?>
                        </div>

                    </div>




            <?php
                    $count++;
                endwhile;
            else :
            // no rows found
            endif;


            ?>
        </div>
    </div>

</div>

<?php
        if($link_receita){ ?>
                <section class="cta-receitas">
                            <div class="container">
                            
                            <a href="<?php echo $link_receita; ?>" class="link-receitas text-uppercase text-center px-5 py-3 d-none d-lg-block">Clique aqui para ver receitas com este produto</a>
                            <a href="<?php echo $link_receita; ?>" class="link-receitas text-uppercase text-center px-5 py-3 d-block d-lg-none">Conheça as receitas</a>

                            </div>
    </section>
    <?php    }
    ?>
<div class="position-relative d-none d-md-block">

    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/path-red.png" class="position-absolute right-0">

</div>


<?php
if (have_rows('compostos__aplicacoes') && get_field('tipo_do_produto') != 'Simples') :

?>
    <div class="container pt-md-5 sub-session">

        <div class="row">

            <h2 class="col-12 color-red mb-5 text-center font-weight-bold">
                <?= $tipo_do_produto; ?>
            </h2>


            <?php


            while (have_rows('compostos__aplicacoes')) : the_row();
                // Get parent value.
                $nome_composto = get_sub_field('nome_do_composto');
                $imagem_composto = get_sub_field('image_do_composto');
                $descricao = get_sub_field('descricao');



            ?>


                <div class="col-md-6 my-4">

                    <div class="row">


                        <div class="col-md-4 text-center text-md-right">

                            <img class="borderd" src="<?= $imagem_composto ?> " alt="">

                        </div>


                        <div class="col-md-6 text-center text-md-left">

                            <div class="col-12 item-title">

                                <?= $nome_composto ?>

                            </div>

                            <div class="col-12">

                                <?= $descricao ?>

                            </div>

                        </div>


                    </div>

                </div>



        <?php

            endwhile;
        else :
        // no rows found
        endif

        ?>



        </div>

    </div>

    

    <div class="position-relative">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/path-left-red.png" class="position-absolute path-left">

    </div>
    
    <!-- 
        <section class="receitas">

            <div class="container py-5 my-5 ">

                <div class="row py-4 ">

                    <div class="col-12 title text-center color-red">
                        Receitas
                    </div>

                    <div class="col-12">

                        <div class="owl-carousel owl-theme carousel-receitas">

                            <div class="item">

                                <div class="thumb">
                                    <img src="https://via.placeholder.com/445x340" alt="">
                                </div>

                                <div class="title">
                                    Nome Receita
                                </div>

                            </div>
                            <div class="item">

                                <div class="thumb">
                                    <img src="https://via.placeholder.com/445x340" alt="">
                                </div>

                                <div class="title">
                                    Nome Receita
                                </div>

                            </div>
                            <div class="item">

                                <div class="thumb">
                                    <img src="https://via.placeholder.com/445x340" alt="">
                                </div>

                                <div class="title">
                                    Nome Receita
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </section> -->

    <section>

        <div class="container">

            <div class="row align-items-center">

                <h2 class="col-md-12 font-weight-bold color-red text-center my-4">Produtos relacionados</h2>

                <div class="col-md-9 m-auto">

                    <div class="owl-carousel owl-theme carousel-produtos-relacionados">

                        <?php

                        $paged = get_query_var('paged') ? get_query_var('paged') : 1;

                        // var_dump($categories[0]);

                        $args = array(

                            'post_type' => 'produtos',


                            'orderby' => 'rand',

                            'order' => 'DESC',

                            'posts_per_page' => '8',

                            'post__not_in'  => array(get_the_ID()),

                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'categoria',
                                    'field'    => 'slug',
                                    'terms'    => $categories[1]->slug
                                )
                            )


                        );

                        $loop = new wp_query($args);

                        while ($loop->have_posts()) : $loop->the_post()
                        ?>


                            <?php

                            while (have_rows('produtos')) : the_row();
                                $sub_value = get_field('foto_externa');
                                $sub_description = get_field('descricao_da_linha');


                            endwhile;


                            ?>


                            <a class="text-center pb-4" href="<?php the_permalink(); ?>">

                                <img style="max-width: 154px;" src="<?= $sub_value['url'] ?>" alt="">

                                <div class="title mt-4">

                                    <?php the_title(); ?>

                                </div>

                                <div class="description color-blue"><?= $sub_description ?></div>

                            </a>

                        <?php endwhile; ?>

                    </div>

                </div>

            </div>

        </div>

    </section>
    
    <section class="bg-red py-4 call-to-action mt-5">

        <div class="container">

            <div class="row">

                <!-- <div class="col-md-6 text-white text-right">Entre em contato<br>conosco agora!</div> -->

                <div class="col-md-12 d-flex justify-content-center">

                    <a class="bg-transparent rounded-pill text-white px-5 py-3 text-uppercase" href="<?= get_site_url(); ?>/fale-conosco/">Entre em contato conosco</a>

                </div>

            </div>


        </div>

    </section>


    </div>

    <?php get_footer(); ?>

    <script>
        $('.carousel-produtos-relacionados').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                }
            }
        });
    </script>