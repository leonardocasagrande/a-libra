<div class="container text-center my-5">

  <div class="row">

    <div class="col-md-6 px-5">

      <img src="https://via.placeholder.com/590x520" alt="">

    </div>

    <div class="col-md-6">

      <div class="title">

        <?= the_title() ?>

      </div>

      <div class="sub-title">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra orci vel urna maximus, nec varius nisl sagittis. Nam et nulla tellus. Donec vestibulum fermentum felis, dignissim molestie.
      </div>

      <div class="description">

        As gorduras lácteas em pó Alibra, desenvolvidas a partir de creme de soro ou creme de leite, são utilizadas como substitutos de manteiga e creme de leite fresco e proporcionam melhoria de textura, aparência, sabor lácteo e cremosidade, além de facilidade de aplicação

      </div>

    </div>

    <div class="col-md-6 my-3">

      <img src="<?= $selo ?>">

    </div>

    <div class="col-md-6 text-center">

      <h3 class="color-blue"> Diferenciais</h3>

    </div>

  </div>

</div>