<?php get_header(); ?>

<div class="container mt-5 pt-4">

  <?php
  $categories = get_categories(array(
    'orderby' => 'name',
    'order'   => 'ASC',
    'parent' => 0
  ));
  ?>

  <div class="row">

    <div class="ftsz-26 color-dgrey mt-n5 pl-4">Categorias:</div>
    <div class="blog-nav-menu d-lg-none">
      <div class="dropdown px-4" role="tablist">
        <button class="btn-dropdown-menu dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Selecione a categoria
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <ul class="nav nav-tabs flex-column">
          <li>
            <a class="nav-link" id="nav-blog-all" data-toggle="tab" href="#tab-blog-all" role="tab" aria-controls="all" aria-selected="true">Todos</a>
          </li>
        <?php foreach ($categories as $category) { ?>

          <li>
            <a class="dropdown-item" id="nav-blog-<?= $category->term_id ?>" data-toggle="tab" href="#tab-blog-<?= $category->term_id ?>" role="tab" aria-controls="tab-blog-<?= $category->term_id ?>" aria-selected="false"> <?= $category->name ?></a>
          </li>


          

        <?php } ?>
        </ul>
        </div>
      </div>
    </div>
    <div class="blog-nav-menu" id="tile-1">

      <ul class="nav nav-tabs nav-justified  d-none d-lg-flex" role="tablist">

        <div class="slider"></div>

        <li class="nav-item">

          <a class="nav-link active" id="nav-blog-all" data-toggle="tab" href="#tab-blog-all" role="tab" aria-controls="all" aria-selected="true">Todos</a>
        
        </li>

        <?php foreach ($categories as $category) { ?>

        <li class="nav-item">

          <a class="nav-link" id="nav-blog-<?= $category->term_id ?>" data-toggle="tab" href="#tab-blog-<?= $category->term_id ?>" role="tab" aria-controls="tab-blog-<?= $category->term_id ?>" aria-selected="false"> <?= $category->name ?></a>
        
        </li>

        <?php } ?>

      </ul>


      <div class="tab-content pt-4 pb-5">
          <!-- tab-pane todos -->
        <div class="tab-pane fade show active" id="tab-blog-all" role="tabpanel" aria-labelledby="nav-blog-all">
          <div class="row posts-blog">
          
            <?php $catquery = new WP_Query("cat=0&posts_per_page=-1"); ?>          
            <?php while ($catquery->have_posts()) : $catquery->the_post(); ?>
            <a href="<?php the_permalink() ?>" title="">
            <div class="col-md-6 mb-4">

              <div class="col-12">

              <?php

              $thumbnail = get_the_post_thumbnail_url();

              if (strlen($thumbnail) == 0) {

                $thumbnail = "https://via.placeholder.com/1024x380";
              } ?>

                <img src="<?= $thumbnail ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">

              </div>

              <div class="col-12">

                <div class="date pl-5 pt-3">

                  <?= get_the_date('d/m/Y'); ?>

                </div>

                <div class="title px-5">

                  <?php the_title(); ?>

                </div>

                <div class="description py-2 px-5">

                <?= the_excerpt(); ?>

                </div>

                <a class="read-more px-5" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"> Continuar Lendo <i class="fas fa-arrow-right"></i></a>

              </div>              

            </div></a>
            <?php endwhile;
            wp_reset_postdata();
            ?>
          </div>
        </div>
          <!-- /tab-pane todos -->

        <?php foreach ($categories as $category) { ?>

        <?php if($category->term_id == 41 || $category->term_id == 48 || $category->term_id == 47): ?>


          <div class="tab-pane fade" id="tab-blog-<?= $category->term_id ?>" role="tabpanel" aria-labelledby="nav-blog-<?= $category->term_id ?>">
            <div class="row posts-blog" id="accordionExample-<?= $category->term_id ?>">
              <p class="title col-12 text-center">Selecione o seu idioma de preferência...</p>
            <?php $cat_filhas = get_categories(array('child_of' => $category->term_id)); 
              foreach($cat_filhas as $cat_filha):
                // var_dump($cat_filha);
            ?>
              <div class="col-lg-4 bg-linguas">
                <a  class=" rounded-pill d-block text-center text-white px-5 py-3 mb-4" data-toggle="collapse" href="#<?= $cat_filha->slug ?>" role="button" aria-expanded="false" aria-controls="<?= $cat_filha->slug ?>"><?= $cat_filha->name ?></a>
              </div>
              <div class="col-12 order-2 collapse row multi-collapse" id="<?= $cat_filha->slug ?>" data-parent="#accordionExample-<?= $category->term_id ?>">
                <?php $argsChildCat = array(
                  'post_type' => 'post',
                  'posts_per_page' => -1,
                  'cat' => $cat_filha->term_id
                );
                $postsChildCat = new WP_Query($argsChildCat);
                if($postsChildCat->have_posts()): while($postsChildCat->have_posts()): $postsChildCat->the_post(); ?>
                  <a href="<?php the_permalink() ?>" title="">
            <div class="col-md-6 mb-4">

              <div class="col-12">

              <?php

              $thumbnail = get_the_post_thumbnail_url();

              if (strlen($thumbnail) == 0) {

                $thumbnail = "https://via.placeholder.com/1024x380";
              } ?>

                <img src="<?= $thumbnail ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">

              </div>

              <div class="col-12">

                <div class="date pl-5 pt-3">

                  <?= get_the_date('d/m/Y'); ?>

                </div>

                <div class="title px-5">

                  <?php the_title(); ?>

                </div>

                <div class="description py-2 px-5">

                <?= the_excerpt(); ?>

                </div>

                <a class="read-more px-5" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"> Continuar Lendo <i class="fas fa-arrow-right"></i></a>

              </div>              

            </div></a>
                <?php endwhile; endif; wp_reset_postdata(); ?>
              </div>
            <?php endforeach; ?>

            </div>
          </div>




        

        <?php else: ?>
          <div class="tab-pane fade" id="tab-blog-<?= $category->term_id ?>" role="tabpanel" aria-labelledby="nav-blog-<?= $category->term_id ?>">
          
          <div class="row posts-blog">

          <?php $catquery = new WP_Query("cat=$category->term_id&posts_per_page=-1");

          while ($catquery->have_posts()) : $catquery->the_post();
          ?>
          <a href="<?php the_permalink() ?>" title="">
          <div class="col-md-6 mb-4">

            <div class="col-12">
              <?php

              $thumbnail = get_the_post_thumbnail_url();

              if (strlen($thumbnail) == 0) {

                $thumbnail = "https://via.placeholder.com/1024x380";
              } ?>

              <img src="<?= $thumbnail ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">

            </div>

            <div class="col-12">

              <div class="date pl-5 pt-3">

                <?= get_the_date('d/m/Y'); ?>

              </div>

              <div class="title px-5">

                <?php the_title(); ?>

              </div>

              <div class="description py-2 px-5">

                <?= the_excerpt(); ?>

              </div>

              <a class="read-more px-5" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"> Continuar Lendo <i class="fas fa-arrow-right"></i></a>

            </div>              

          </div></a>

          <?php endwhile;
          // wp_reset_postdata();
            ?>         
            
          </div>

        </div>
        <?php endif; ?>
        <?php } ?>

      </div>

    </div>

  </div>

</div>

<?php get_footer(); ?>