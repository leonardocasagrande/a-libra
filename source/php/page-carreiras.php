<?php get_header('carreira'); ?>

<div class="bg-carreiras text-center full-center">

  <div class="container text-white">

    <h1>Empresa jovem, <br>
      espírito inovador.</h1>

    <h2>A inspiração está no nosso DNA!</h2>

    <div class="mt-5">

      <a href="http://site.vagas.com.br/alibra?url=http://www.vagas.com.br/alibra&t=v" target="_blank" class="text-white border rounded-pill py-3 px-5 font-weight-bold">

        VENHA FAZER PARTE DESSE TIME

      </a>

    </div>

  </div>

</div>

<div class="container">

  <h4 class="color-green px-3 text-center p-3 mxw-1000 font-weight-bold pt-4 m-auto">

    Se você se identificou com a Alibra e deseja fazer parte da empresa, aqui você pode conferir as vagas em aberto.

  </h4>

</div>

<div class="container my-3">

  <div class="row my-4 align-items-center">

    <div class="col-6">

      <p>Empresa nacional, especializada no fornecimento de ingredientes, misturas alimentícias em pó e substitutos de queijos para o mercado de alimentos e de bebidas.</p>

      <p class="mb-5">Possui um extenso portfólio de produtos, dentre os quais se destacam os compostos lácteos, utilizados em diferentes aplicações como substitutos de leite em pó.</p>

      <!-- <a class="bg-blue py-3 px-4 rounded-pill text-white font-weight-bold mt-2" href="http://site.vagas.com.br/alibra?url=http://www.vagas.com.br/alibra&t=v">VENHA FAZER PARTE DESSE TIME! </a> -->

    </div>

    <div class="col-md-6">

      <div class="owl-carreira owl-carousel owl-theme">

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/matriz-alibra.jpg">

        </div>

        <!-- <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/fabrica-alibra.jpg">

        </div> -->

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/fabrica-alibra2.jpg">

        </div>



      </div>


    </div>


  </div>



</div>

<!-- 
<div class="bg-light py-5">

  <div class="container">

    <div class="d-flex flex-md-reverse align-items-center ">

      <div class="col-md-6">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/carreira-img-2.webp">


      </div>

      <div class="col-md-6">

        <p>A Alibra Ingredientes iniciou suas operações na região Metropolitana de Campinas/SP em 2000 e, em 2002, inaugurou uma moderna fábrica em Marechal Cândido Rondon/PR que conta hoje com quatro torres de secagem de última geração.

          Resultado de uma sólida formação técnica dos sócios com know-how adquirido em grandes multinacionais no setor de alimentos, a empresa distribui seus produtos para todo o território nacional e a diversos países.

          Busca, de forma incansável, criar soluções inovadoras para atender as necessidades de seus clientes. Sua expertise de secagem de misturas líquidas, através do processo spray-dryer, possibilita atender a mercados distintos, tais como: indústrias de alimentos, food service, supermercados e atacadistas.</p>


      </div>

    </div>

  </div>

</div> -->




<?php get_footer(); ?>