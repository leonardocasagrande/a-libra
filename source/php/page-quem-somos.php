<?php get_header(); ?>




<section class="section-title">

  <div class="container default-spacing">
    <div class="row align-items-center">
      <div class="col-md-8 text-md-left text-center">
        <h2 class="title-about px-3 px-md-0 class-spacing">Empresa jovem, espírito inovador.</h2>
        <p class="paragraph-format px-3 px-md-0">
          Fundada em 2000, a Alibra Ingredientes é uma empresa nacional, especializada em fornecer ingredientes lácteos e não lácteos, misturas alimentícias em pó e soluções alternativas aos queijos para o mercado de alimentos e de bebidas.

        </p>
      </div>

      <div class="col-md-4 d-flex justify-content-center mt-4">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/alibra-img.webp" class="img-fluid">
      </div>
      <img class="d-none d-lg-block right-0 mt-5-1 position-absolute" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/azul.webp">
    </div>
  </div>
</section>

<section class="section-fabrica bg-blue my-md-5 mt-5">
  <div class="container-fluid pl-md-0">
    <div class="row align-items-center">
      <div class="col-md-4 pl-md-0 mt-sm-n5 text-md-left text-center">
        <img class="img-fluid img-fabrica my-md-n5" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/img-fabrica.webp">
      </div>
      <div class="col-md-8">
        <p class="paragraph py-3 col-md-8 ml-md-5 text-md-left text-center text-white"><strong class="color-green">Com mais de 300 colaboradores</strong>, atualmente é referência no fornecimento de ingredientes para indústria alimentícia de todos os portes no Brasil e no exterior, além de atuarmos nos segmentos Food Service e Varejo.</p>
      </div>
    </div>
  </div>
</section>

<?php

include 'onde-atuamos.php'

?>

<section class="section-exportacao full-center py-md-5 py-4  bg-alabastera text-md-left text-center">
  <div class="container bg-mapa h-100 py-md-5">
    <h2 class="font-weight-bold text-center color-green py-3 ">Exportação</h2>
    <div class="row justify-content-center">
      <p class="paragraph-exportacao col-md-8 m-auto text-center">
        A Alibra atua no mercado internacional, fornecendo seus produtos com a seriedade e a regularidade comercial exigida pelos órgãos de cada um dos países compradores. Atualmente exporta seus produtos para América Latina, África, Oriente Médio, além do Caribe.</p>


    </div>
    <div class="row align-items-center">
      <div class="col-md-6 d-flex justify-content-center py-4">
      </div>
      <div class="col-md-6 d-flex justify-content-center">
      </div>
    </div>
  </div>
</section>

<section class="section-localidades pt-2 text-md-left text-center pb-3">
  <div class="container">
    <h2 class="title-section text-center py-3" style="padding-top: 30px !important;">Localização</h2>
    <p class="text-center py-3">Com duas unidades estrategicamente localizadas, em Campinas/SP e Marechal Cândido Rondon/PR, a Alibra conta com uma estrutura fabril e logística capaz de fornecer insumos e produtos finais para todas as regiões do Brasil e para diversos países.</p>
    <div class="row align-items-center flex-wrap-reverse flex-md-wrap">
      <div class="col-md-5 py-3">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/img-localidade-1.webp">
      </div>
      <div class="col-md-6">
        <p class="paragraph-color">Matriz | Campinas/SP</p>
        <span class="span-format">Área administrativa e Centro de Inovação e Aplicação</span> <br> <br>
        <span class="span-format">A sede administrativa da Alibra e o Centro de Inovação, estão localizados estrategicamente em Campinas-SP, no condomínio empresarial Techno Park.</span>
      </div>
    </div>


    <div class="row align-items-center">
      <div class="col-md-6 py-3">
        <p class="paragraph-color">Filial | Marechal <br>Cândido Rondon/PR</p>
        <span class="span-format">A unidade fabril está situada em Marechal Cândido Rondon-PR, com uma área própria de 52 mil m² e em constante processo de ampliação.</span> <br>

      </div>
      <div class="col-md-5 index">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/img-campo.webp" class="img-fluid pb-5">
      </div>

      <img class="d-none d-lg-block right-0 position-absolute mt-n5-1" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-way.webp">
    </div>
  </div>
</section>

<section class="section-linha bg-dgrey text-md-left text-center">
  <div class="container">
    <h2 class="py-30 text-center color-green font-weight-bold">Linha do tempo</h2>
    <div class="col-md-12" style="padding-bottom: 44px;">

      <div class="owl-carousel owl-theme" id="about-us">

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2000</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">Fundação da Alibra</h5>
              <p class="card-text">Fundação da Alibra, ainda na cidade de Valinhos/SP, com foco no mercado B2C e no atendimento aos atacadistas, distribuidores e montadores de cestas básicas.</p>
            </div>
          </div>
        </div>

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2003</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">Inauguração</h5>
              <p class="card-text">Inauguração da fábrica em Marechal Cândido Rondon/PR.</p>
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2003.png" class="img-linha">
            </div>
          </div>
        </div>

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2004</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">Transferência</h5>
              <p class="card-text">Fábrica de Valinhos/SP é transferida para Campinas/SP.</p>
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2004.png" class="img-linha">
            </div>
          </div>
        </div>

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2005</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">B2B</h5>
              <p class="card-text">Início da atuação no mercado B2B (ingredientes lácteos e não lácteos).</p>
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2005.png" class="img-linha">
            </div>
          </div>
        </div>

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2008</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">Novas Torres</h5>
              <p class="card-text">Início da operação de duas novas torres de secagem em Marechal Cândido Rondon/PR.</p>
              <img class="img-linha" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2008-torres.png">

            </div>
          </div>
        </div>

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2009</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">Queijos Modificados</h5>
              <p class="card-text">Lançamento da linha de queijos modificados.</p>
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2009.png" class="img-linha">
            </div>
          </div>
        </div>

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2012</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">Quarta Torre</h5>
              <p class="card-text">Início da operação da quarta torre de secagem em Marechal Cândido Rondon/PR.</p>
              <img class="img-linha" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2012Quartatorre.png">
            </div>
          </div>
        </div>

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2015</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">Genkor</h5>
              <p class="card-text">Incorporação da Genkor, empresa de ingredientes focada no mercado industrial.</p>
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2015.png" class="img-linha">
            </div>
          </div>
        </div>
        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2016</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">UHT</h5>
              <p class="card-text">Inauguração da planta piloto UHT (Ultra High Temperature) em Campinas/SP.</p>
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2016-UHT.png" class="img-linha">
            </div>
          </div>
        </div>

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2018</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">Transferência</h5>
              <p class="card-text">Transferência de Campinas/SP para a fábrica de Marechal Cândido Rondon/PR.</p>
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2018-Transferência.png" class="img-linha">
            </div>
          </div>
        </div>

        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card ftz-30">2018</div>
            <div class="card-body">
              <h5 class="card-title color-blue ftz-20">CIA</h5>
              <p class="card-text">Inauguração do Centro de Inovação e Aplicação e novo escritório em Campinas/SP.</p>
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2018-CIA.png" class="img-linha">
            </div>
          </div>
        </div>



      </div>
      <div class="row justify-content-between align-items-center">
        <div id="materiaDots" class="owl-dots col-md-2 m-auto my-4 my-md-0 d-flex"></div>
      </div>
    </div>
  </div>
</section>

<section class="section-solucoes text-md-left text-center">
  <div class="container">
    <div class="row align-items-center justify-content-around position-relative py-md-5 py-4">

      <div class="col-md-6">
        <h2 class="title-solucoes text-md-left text-center ftz-30 py-30">Soluções <br> Sob Medida</h2>
        <p class="text-md-left text-center paragraph-solucoes">
          Em todos segmentos em que atua, a Alibra desenvolve soluções customizadas, inclusive com apelo à saudabilidade.


        </p>
        <p class="text-md-left text-center paragraph-solucoes">

          Nos segmentos Industrial e Food Service, as soluções são desenvolvidas de acordo com os perfis e as necessidades de aplicação dos clientes, possibilitando que o produto final adquira as características desejadas.
        </p>
        <p class="text-md-left text-center paragraph-solucoes">
          A Alibra também atua no setor de terceirização com qualquer item do portfólio de produtos.

        </p>
      </div>
      <div class="col-md-4 pr-md-0 justify-content-end">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/solucoes.webp" class="img-fluid" style="    border-radius: 30px;">
      </div>
    </div>
  </div>
</section>

<section class="section-premiacoes text-md-left text-center">
  <div class="col-md-4 col-2 d-none d-lg-block pl-0 position-absolute">
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/formas.webp" class="img-fluid">
  </div>
  <div class="container">
    <h2 class="text-center py-30 title-premiacoes ftz-30">Premiações</h2>
    <p class="text-center col-md-10 m-auto">

      Os resultados do desafio diário na busca pela excelência vêm sendo reconhecidos por proeminentes canais de comunicação do segmento. <br>

      Muito mais que números, para a Alibra, toda premiação contempla o comprometimento e profissionalismo presentes em todos os seus processos, o que enaltece ainda mais cada conquista.
    </p>
    <div class="row justify-content-center py-3">
      <div class="col-md-6">
        <p class="text-center py-30 paragraph-premiacoes ftz-20">Melhores do agronegócio</p>
        <div class="row col-md-11 m-auto">
          <!-- Início Mobile -->
          <div class="col-md-3 d-md-none d-sm-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/premiacoes-1.webp" class="img-fluid">
          </div>
          <div class="col-md-3 d-md-none d-sm-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/premiacoes-2.webp" class="img-fluid">
          </div>
          <div class="col-md-3 d-md-none d-sm-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/premiacoes-3.webp" class="img-fluid">
          </div>
          <div class=" d-md-none d-flex align-items-center ">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/premiacoes-4.png" class="img-fluid col-6">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/premiacoes-5.png" class="img-fluid col-6">
          </div>
          <!-- Fim Mobile -->
          <div class="col-md-4 d-none d-md-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2014.webp" class="img-fluid">
          </div>
          <div class="col-md-4 d-none d-md-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2015.webp" class="img-fluid">
          </div>
          <div class="col-md-4 d-none d-md-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2016.webp" class="img-fluid">
          </div>
          <div class="col-md-4 d-none d-md-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2017.webp" class="img-fluid">
          </div>
          <div class="col-md-4 d-none d-md-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2018.webp" class="img-fluid">
          </div>
          <div class="col-md-4 d-none d-md-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2019.webp" class="img-fluid">
          </div>
          <div class="col-md-4 d-none d-md-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2020.png" class="img-fluid">
          </div>
          <div class="col-md-4 d-none d-md-block">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/premiacoes-5.png" class="img-fluid">
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <p class="text-center py-4 paragraph-premiacoes">PME’s que mais crescem no Brasil</p>
        <div class="row col-md-8 m-auto">
          <div class="col-md-12">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pme-1.webp" class="img-fluid">
          </div>
          <div class="col-md-12">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pme-2.webp" class="img-fluid">
          </div>

        </div>


      </div>

    </div>

  </div>
</section>
<section class="section-politica section-premiacoes bg-dblue">
  <div class="container">
    <div class="row">
      <div class="col-12 d-lg-none text-center mb-4"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/qualidade.png" alt=""></div>
      <div class="col-12 col-lg-8 bloco-qualidade">
        <h2 class="ftz-30 color-green">Política da Qualidade e Segurança dos Alimentos</h2>
        <h3 class="color-green">MISSÃO:</h3>
        <p>
          Oferecer soluções integradas em produtos e ingredientes alimentícios seguros, com qualidade e pontualidade, atendendo às necessidades do mercado e promovendo o crescimento sustentável da organização.
        </p>
        <h3 class="color-green">COMPROMISSOS:</h3>
        <p>
          1 - Garantir produtos seguros pelo emprego do conhecimento técnico e do monitoramento de processos, produtos e serviços;<br><br>

          2 - Fortalecer as relações com todos os seus parceiros, disponibilizando recursos humanos e técnicos, na busca de melhores soluções;<br><br>

          3 - Capacitar nossos colaboradores, compartilhando o conhecimento e promovendo a busca por desenvolvimento técnico e pessoal;<br><br>

          4 - Incentivar as relações interpessoais e o reconhecimento do valor dos colaboradores como ferramentas fundamentais da qualidade;<br><br>

          5 - Adotar novas tecnologias objetivando a melhoria contínua de processos e produtos;<br><br>

          6 - Cumprir com os requisitos estatutários, regulamentares e de clientes quanto à segurança dos alimentos, do meio ambiente e da saúde e segurança dos colaboradores;<br><br>

          7 - Utilizar a comunicação interna e externa como importante ferramenta para o desenvolvimento da organização.
        </p>
      </div>
      <div class="col-12 col-lg-4 d-none d-lg-block">
        <img class="img-fluid mb-4" src="<?= get_stylesheet_directory_uri() ?>/dist/img/qualidade.png" alt="">
        <img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/qualidade-DSCF603.png" alt="">
      </div>
      <div class="col-12 d-lg-none text-center mt-4">
        <img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/qualidade-DSCF603.png" alt="">
        <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/efeito-verde-esquerda.png" alt="" class="efeito-verde-esquerdaMob">
      </div>
      <div class="col-12 col-lg-4 d-none d-lg-block bloco-ambiental"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/ESG-DSCF1825.png" alt=""></div>
      <div class="col-12 col-lg-8 bloco-ambiental">
        <h2 class="ftz-30 color-green">Política Ambiental Alibra</h2> 
        <p>
          A Alibra Ingredientes S.A. desenvolve seus produtos pensando não somente nos seus clientes, mas também no planeta como um todo, o que faz da sustentabilidade um valor importante para a organização.
        </p>
        <h3 class="color-green">Nossa Política Ambiental baseia-se em:</h3>
        <p>
          Prevenir e controlar a poluição gerada nas atividades da empresa, com ênfase em efluentes líquidos, resíduos sólidos e emissões atmosféricas; 
        </p>
        <p>
          Assegurar o cumprimento da legislação, normas ambientais e demais requisitos aplicáveis; <br>
          Melhorar continuamente o desempenho ambiental da empresa através do Sistema de Gestão Ambiental (SGA).
        </p>
      </div>
      <div class="col-12 d-lg-none text-center"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/ESG-DSCF1825.png" alt=""><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/efeito-verde-direita.png" alt="" class="efeito-verde-direitaMob"></div>
    </div>
    <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/efeito-verde-direita.png" alt="" class="efeito-verde-direitaDesk d-lg-block d-none">
    <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/efeito-verde-esquerda.png" alt="" class="efeito-verde-esquerdaDesk d-lg-block d-none">
  </div>
</section>



<section class="section-premiacoes ">
  <div class="container">

    <h2 class="text-center pt-3 title-premiacoes">Certificações</h2>

    <div class="d-flex flex-wrap  align-items-center  justify-content-center">
      <div class="col-md-5 d-flex pb-lg-0 pr-lg-5 justify-content-center">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Logo-FSSC.png">
      </div>
      <div class="col-md-2 col-6 d-block d-lg-flex justify-content-center py-4">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exportacao-1.webp">
      </div>
      <div class="col-md-2 d-flex col-6 py-5 py-lg-0 justify-content-center">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exportacao-3.webp">
      </div>
    </div>

    <span class="mini-desc">A Alibra foi certificada pela FSSC 22000 para a linha de soluções alternativas aos queijos</span>

    <div class="row align-items-center text-md-left text-center">

      <div class=" ">A Alibra preza pela transparência, excelência e integridade nas relações comerciais e, por isso, busca incansavelmente a eficiência inovadora e investe constantemente na melhoria de todos os processos com o objetivo de fornecer soluções diversificadas e de qualidade ao mercado de alimentos e de bebidas. As certificações obtidas representam o compromisso de melhoria contínua com clientes e parceiros.

        <p>Por meio da sua expertise e do alto investimento em pesquisa, inovação e desenvolvimento, a empresa conquistou as certificações que dão o direito a desenvolver e fornecer seus produtos também para países muçulmanos e para o mercado judaico.</p>

      </div>

    </div>


    <!-- <hr class="hr-color mb-5">


    <div class="row align-items-center mb-4 text-md-left text-center">

      <div class="col-md-6">

        <p>

          A Alibra está em processo de Certificação da Norma FSSC 22000 - Food Safety System Certification - na unidade industrial da Alibra Ingredientes, situada em Marechal Cândido Rondon-PR.

        </p>

        <p>

          Esta certificação internacional, que tem como objetivo monitorar e garantir a segurança na produção e na distribuição de alimentos de empresas que processam e fabricam produtos alimentícios e embalagens para alimentos, garantirá aos clientes e consumidores da Alibra ainda mais credibilidade e confiabilidade nos quesitos de segurança e de qualidade dos alimentos e ingredientes desenvolvidos, colaborando, desta forma, para o avanço da segurança dos alimentos em âmbito global, já que o processo envolve diversos elos da cadeia produtiva.

        </p>

        <p>

          A obtenção da certificação FSSC 22000 pela Alibra está prevista para o 2º semestre de 2020.

        </p>

      </div>

      <div class="col-md-6 text-center">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo_fsc22000.png">

      </div>

    </div> -->

  </div>
</section>



<section class="section-cia py-4 py-md-5 text-md-left text-center">
  <div class="container">
    <div class="row align-items-center">

      <div class="col-md-8 pl-0">
        <h2 class="title-cia">CIA - Centro de <br>
          Inovação e Aplicação</h2>
        <p>
          Localizado na matriz, em Campinas - SP, o CIA Alibra conta com laboratórios modernos e com o apoio de duas plantas piloto de última geração, uma delas voltada para o segmento de Lácteos e outra ao segmento de Gelados Comestíveis. Nestas plantas, engenheiros e cientistas de alimentos e tecnólogos em lácteos trabalham no desenvolvimento de produtos e soluções inovadoras.


          <br><br>Estes profissionais somam conhecimento acadêmico, vivência industrial e experiência de mercado, além de um olhar crítico sobre tendências de consumo, o que permite o desenvolvimento de novos produtos, otimização de custos e processos, além da oferta de soluções inovadoras ao mercado.
        </p>
      </div>
      <div class="col-md-4 d-flex justify-content-md-end pr-md-0 bordered">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cia.webp">
      </div>
    </div>
  </div>
</section>

<section class="section-planta bg-blue py-4 py-md-5">
  <div class="container">
    <h2 class="title-plant text-center text-white pb-md-5 pb-3 ">Planta Piloto UHT</h2>
    <div class="row align-items-center">
      <div class="col-md-6">
        <div class="owl-carousel owl-theme" id="plant">
          <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lab.webp" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lab-2.webp" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lab-3.webp" class="img-fluid">
          </div>
        </div>
      </div>
      <div class="col-md-6 text-md-left text-center">
        <p class="paragraph-plant text-white">A Planta Piloto UHT Alibra simula condições de processos existentes em diferentes plantas industriais, tornando o desenvolvimento de produtos mais rápido e assertivo.
          <br><br>

          De última geração, possui controle digital, o que garante alta precisão e permite ampla flexibilidade de processos para diversos produtos dentro do segmento de UHT.

          <br><br>

          Sua característica marcante é o grau de representatividade de resultados, em escala piloto, antes de se partir para testes industriais, que envolvem demandas expressivas de tempo de produção e volumes de matérias-primas. Com o domínio dessa tecnologia, os riscos de insucessos são minimizados pelo maior grau de assertividade e, consequentemente, menores prazos para conclusão de projetos. Na Planta Piloto UHT Alibra pode-se avaliar, em média, 6 variações de formulação em um mesmo dia. Com protótipos envasados assepticamente em garrafas PET irradiadas, é possível que esses sejam analisados durante o shelf life.

        </p>

      </div>
    </div>
    <!-- <img class="d-none d-md-block right-0 position-absolute" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/way-green.webp">
  </div> -->
</section>

<?php get_footer(); ?>