<?php 
global $header_title;

if(is_null($header_title)){

    $header_title = get_the_title(); 

}

?>
<div class="container h-100">

    <div class="row full-center h-100">

        <div class="col-md-6 text-md-left text-center">

        <h1 class="title">

            <?= $header_title ?>

        </h1>

        </div>

        <div class="col-md-6 d-none d-md-block">

        <?php
        if (function_exists('yoast_breadcrumb')) {
            yoast_breadcrumb('<div id="breadcrumbs">', '</div>');
        }
        ?>

        </div>

    </div>

</div>