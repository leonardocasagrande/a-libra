<section>

  <div class="container mt-3 mt-md-0">

    <h2 class="title-where text-center">Onde atuamos</h2>

    <div class="col-lg-10 m-auto">

      <p class="paragraph-where text-center paragraph-height mb-0">

        Com flexibilidade para atender às diferentes demandas do mercado de alimentos, a Alibra conta com um extenso portfólio de soluções desenvolvidas para indústrias de alimentos e de bebidas de diversos segmentos, para o mercado Food Service e Varejo. Além de produzir e desenvolver marcas próprias para clientes.

      </p>

    </div>

    <img class="d-none d-lg-block left-0 mt-n5 position-absolute" src="https://alibra.com.br/wp-content/themes/alibra/dist/img/verde.webp">

    <div class="row default-spacing align-items-end actuation">

      <div class="col-lg-4 text-center d-flex flex-wrap my-4 my-lg-0">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-food-service.webp" class=" m-auto" style="max-height: 92px">


        <h4 class="title-food text-center w-100">Food Service</h4>

        <p class="paragraph-default text-center paragraph-height">

          Fornecimento de ingredientes e produtos para o mercado de alimentação fora do lar, como: pizzarias, lanchonetes e restaurantes.

        </p>

        <div class="col-md-8 m-auto">

          <a class="nav-link bg-blue rounded-pill text-white px-4 py-3" href="<?= get_site_url(); ?>/produtos/#food-service" class="bg-green text-uppercase"> Ver produtos</a>
        </div>
      </div>

      <div class="col-lg-4 text-center d-flex flex-wrap my-4 my-lg-0">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-industria-alimentos.webp" class=" m-auto" style="max-height: 92px">

        <h4 class="title-industria text-center w-100">Indústria de alimentos e de bebidas</h4>
        <p class="paragraph-default text-center paragraph-height">Fornecimento de ingredientes para indústrias de alimentos e bebidas, de pequeno, médio e grande porte, dos mais variados ramos de atuação.</p>

        <div class="col-md-8 m-auto">
          <a class="nav-link bg-blue rounded-pill text-white px-4 py-3" href="<?= get_site_url(); ?>/produtos/#industria-de-alimentos-e-de-bebida" class="bg-green text-uppercase"> Ver produtos</a>
        </div>
      </div>

      <div class="col-lg-4 text-center d-flex flex-wrap my-4 my-lg-0">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-varejo.webp" class="m-auto" style="max-height: 92px">

        <h4 class="title-varejo text-center w-100">Varejo</h4>

        <p class="paragraph-default text-center paragraph-height">Fornecimento de compostos lácteos, achocolatados, farinha láctea e cereais em pó para atacadistas, supermercados
          e montadoras de cestas básicas.</p>

        <div class="col-md-8 m-auto">

          <a class="nav-link bg-blue rounded-pill text-white px-4 py-3" href="<?= get_site_url(); ?>/produtos/#varejo" class="bg-green text-uppercase"> Ver produtos</a>

        </div>

      </div>

    </div>
    <img class="d-none d-lg-block right-0 mt-n5-1 position-absolute" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/amarelo.png">
  </div>

</section>