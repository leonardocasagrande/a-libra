<?php get_header(); ?>






<?= do_shortcode('[rev_slider alias="home"]') ?>

<div class="position-relative">

  <div class="d-flex align-items-center justify-content-between">

    <div class="d-none d-lg-block col-4 pl-0">

      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/predio.png" alt="Prédio representando a sede da A Libra" title="Prédio representando a sede da A Libra" class="h-100">

    </div>

    <div class="col-lg-4 pl-md-6 my-4 pb-3 text-center text-lg-left">

      <h2 class="color-blue"><b> Sobre nós</b></h2>

      <p class="pb-4 class-spacing">
        Fundada em 2000, a Alibra Ingredientes é uma empresa nacional, especializada em fornecer ingredientes lácteos e não lácteos, misturas alimentícias em pó e soluções alternativas aos queijos para o mercado de alimentos e de bebidas.
        Com mais de 300 colaboradores, atualmente é referência no fornecimento de ingredientes para indústria alimentícia de todos os portes no Brasil e no exterior, além de atuarmos nos segmentos Food Service e Varejo.</p>

      <a class="nav-link d-inline bg-green rounded-pill text-white px-5 py-3" href="<?= get_site_url(); ?>/quem-somos/">SAIBA MAIS</a>

    </div>

    <div class="col-md-3 col-2 d-none d-lg-block  text-right pr-0">

      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detalhe-azul-verde.svg">

    </div>

  </div>

</div>



<div class="bg-light py-5">

  <div class="container">

    <div class="row justify-content-center align-items-center">

      <div class="col-md-12 text-center">

        <h2 class="color-green class-spacing">Nossas Marcas</h2>


      </div>

      <div class="col-md-7">

        <div class="owl-default marcas owl-carousel owl-theme text-center">

          <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Mozzana_logo.webp">
          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Multmix_logo.webp">


          </div>
          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-alibralac.png">


          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Merilu_Tradicional_logo.png">

          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/genkor lac-01.webp">

          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/GenkorPan.webp">

          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/LaNutre.webp">

          </div>

          <!-- <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/LinhaV.webp">

          </div> -->

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/ALICREM.webp">

          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Merilu_Levinho_logo.png">

          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pizzalet.png" alt="Pizzalet requeijão cremoso" title="Pizzalet requeijão cremoso">

          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mozzalet.png" alt="Mozzalet cobertura para pizza" title="Mozzalet cobertura para pizza">

          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Nutrisim_logo.webp" alt="Nutrisim Composto Lácteo" title="Nutrisim Composto Lácteo">

          </div>

          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Reckmozzana_logo.webp">

          </div>

        </div>

      </div>

    </div>

  </div>

</div>

<?php include('onde-atuamos.php') ?>

<!-- <div class="container home">

  <h2 class="color-dgrey text-center mt-5 pt-3">

    <b>

      Blog

    </b>

  </h2>

  <div class="row">

    <?php

    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

    $args = array(

      'post_type' => 'post',

      'order' => 'ASC',

      'posts_per_page' => '2',

      'paged' => $paged,

    );

    $loop = new wp_query($args);

    while ($loop->have_posts()) : $loop->the_post() ?>

      <a class="col-md-6 p-4" href="<?php the_permalink(); ?>">

        <div class="box-image">

          <div class="img-post-blog" style="background: url('<?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                                              echo $url; ?>') no-repeat center center; background-size:cover;">

          </div>

        </div>

        <div class="box-info">

          <div class="titulo-blog-post color-greyd">

            <div class="color-grey my-2">

              <?php echo get_the_date('d/m/Y'); ?> <br>

            </div>

            <span class="color-blue title-blog">

              <?php the_title(); ?>

            </span>

            <p class="color-grey my-2"> <?php echo wp_trim_words(get_the_content(), 20, '...'); ?></p>

            <div class="color-green" href="">

              Continuar Lendo <i class="fas fa-arrow-right"></i>

            </div>

          </div>

        </div>

      </a>

    <?php endwhile; ?>

  </div>

</div> -->

<?php get_footer(); ?>


