console.log("atualizou ");
jQuery(document).ready(function ($) {
  jQuery("img").removeAttr("width").removeAttr("height");
  $(".wp-caption").removeAttr("style");
});

$(".close-pop").on("click", function (e) {
  e.preventDefault();
  $(".popup-box").remove();
});

$(document).ready(function () {
  $(".blog-nav-menu .nav-tabs a").click(function () {
    var position = $(this).parent().position();

    var width = $(this).parent().width();

    $(".blog-nav-menu .slider").css({ left: +position.left, width: width });
  });

  var actWidth = $(".blog-nav-menu .nav-tabs")
    .find(".active")
    .parent("li")
    .width();

  var actPosition = $(".blog-nav-menu .nav-tabs .active").position();

  $(".blog-nav-menu .slider").css({ left: +actPosition.left, width: actWidth });
});

$(window).scroll(function () {
  if ($(this).scrollTop() > 109) {
    $(".nav-scroll").addClass("fixed-top");
    $(".height-breaf").removeClass("d-none");
  } else {
    $(".nav-scroll").removeClass("fixed-top");
    $(".height-breaf").addClass("d-none");
  }
});

$(".owl-default").owlCarousel({
  loop: false,
  margin: 50,
  nav: false,
  autoplay: true,
  autoplayTimeout: 2000,
  autoplayHoverPause: true,

  responsive: {
    0: {
      items: 1,
      dots: false,
    },
    600: {
      items: 3,
      dots: true,
    },
    1000: {
      items: 3,
    },
  },
});

$(".owl-carreira").owlCarousel({
  loop: false,
  margin: 50,
  nav: false,
  dots: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1000: {
      items: 1,
    },
  },
});

$("#about-us").owlCarousel({
  loop: false,
  margin: 10,
  nav: true,
  navContainer: "#materiaNav",
  dotsContainer: "#materiaDots",
  navText: [
    "<i class='fas fa-arrow-left color-active next color-grey'></i>",
    "<i class='fas color-grey color-active fa-arrow-right ml-3'></i>",
  ],
  responsive: {
    0: {
      items: 1,
      dots: false,
    },
    600: {
      items: 3,
      dots: true,
    },
    1000: {
      items: 4,
    },
  },
});

$("#plant").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1000: {
      items: 1,
    },
  },
});
$(".carousel-receitas").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 3,
    },
  },
});

var SPMaskBehavior = function (val) {
    return val.replace(/\D/g, "").length === 11
      ? "(00) 00000-0000"
      : "(00) 0000-00009";
  },
  spOptions = {
    onKeyPress: function (val, e, field, options) {
      field.mask(SPMaskBehavior.apply({}, arguments), options);
    },
  };

$(".cellphone").mask(SPMaskBehavior, spOptions);

$(document).on("click", ".nav-item.active", function () {
  var href = $(this).attr("href").substring(1);
  $(href).removeClass("active");
  $('.tab-pane[id="' + href + '"]').removeClass("active");
});

$("body").on("click", ".set-cat", function () {
  var clones = null;

  var setcat = $(this).data("setcat");

  var catFather = $(this).data("categoryFather");

  $(".get-cat").addClass("d-none");

  $("#get-cat-" + setcat).removeClass("d-none");

  clones = $(".set-cat-" + catFather).clone();

  $(".set-cat-" + catFather).remove();

  clones.appendTo(".sub-cat-header-" + catFather);

  $(".set-cat-" + catFather).removeClass("set-cat-" + catFather);

  $(".cat-child").addClass("flex-wrap");

  $(".cat-child").removeClass("overflow-x");

  var y = $("#get-cat-"+setcat).position().top;

      $("html, body").animate({ scrollTop: y - 300 });
});

if (window.location.pathname == "/produtos/") {
  var hash = window.location.hash;

  hash = hash.replace("#", "");
  if(hash == "genkorlac" || hash == "ingredientes-para-aplicacoes-diversas" || hash == "multmix" || hash == "solucoes-alternativas-aos-queijos"){
    $(document).ready(function () {
      $("#link-industria-de-alimentos-e-de-bebida").trigger("click");
      
      $("#"+ hash ).trigger("click");

      var y = $("#get-cat-"+hash).position().top;

      $("html, body").animate({ scrollTop: y - 300 });
    });
  }

  $(document).ready(function () {
    $("#link-" + hash).trigger("click");
  });
}

$(".nav-cat").click(function () {
  $(".nav-cat").addClass("britness-cat");
});



$(document).ready(function () {
  $(".cnpj").mask("99.999.999/9999-99");
});

$(".send").click(function (e) {
  var email = $(".email1").value;
  var check = $(".email2").value;
  var responseBox = $(".wpcf7-response-output");

  if (email === check) {
    // var loadMore = $(".send");

    // loadMore.on('click', function () {
    var dataPost = $(this).attr("id");

    document.addEventListener(
      "wpcf7mailsent",

      function (event) {
        var formId = "form-" + event.detail.contactFormId;

        var label = event.detail.inputs[0].value;

        if (dataPost == formId) {
          console.log("entrou no laço");
          gtag("event", "contact", {
            event_category: "Fale-conosco",
            event_action: "click",
            event_label: label,
          });
        }
      },
      false
    );
    // });
  } else {
    e.preventDefault();
    responseBox.classList.toggle("wpcf7-display-none");
    responseBox.innerHTML =
      "Os campos de e-mail não são iguais. Verifique por favor.";
  }
});
