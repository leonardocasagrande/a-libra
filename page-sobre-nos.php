<?php get_header(); ?> <section class="section-title">
  <div class="container py-5">
    <div class="row align-items-center py-5">
      <div class="col-md-8">
        <h2 class="title-about">Empresa Jovem. Espírito Inovador.</h2>
        <p class="paragraph-format">Fundada em 2000, a Alibra é especializada em desenvolver ingredientes para a indústria de alimentos, bebidas e foodservice. A empresa surgiu como resultado de uma sólida formação técnica e da experiência dos sócios que acumulam passagens por grandes multinacionais do setor de alimentos.</p>
      </div>
      <div class="col-md-4 d-flex justify-content-center"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/alibra-img.png" class="img-fluid"></div><img class="d-none d-md-block right-0 mt-5-1 position-absolute" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/azul.png">
    </div>
  </div>
</section>
<section class="section-fabrica bg-blue my-5">
  <div class="container-fluid pl-md-0">
    <div class="row align-items-center">
      <div class="col-md-4 pl-md-0 mt-sm-n5 text-md-left text-center"><img class="img-fluid img-fabrica my-md-n5" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/img-fabrica.png"></div>
      <div class="col-md-8">
        <p class="paragraph py-3">Atualmente <strong class="color-green">composta por mais de 700 colaboradores</strong>, diretos e indiretos, a Alibra é referência em inovação e atende a empresas de todos os portes no Brasil e no exterior.</p>
      </div>
    </div>
  </div>
</section> <?php

            include 'onde-atuamos.php'

            ?> <section class="section-localidades mt-5">
  <div class="container">
    <h2 class="title-section text-center py-5">Localidades</h2>
    <div class="row align-items-center">
      <div class="col-md-6 py-3"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/img-localidade-1.png"></div>
      <div class="col-md-6">
        <p class="paragraph-color">Matriz Campinas - SP</p><span class="span-format">Área administrativa e Centro<br>de Inovação e Aplicação</span>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-md-6 py-3">
        <p class="paragraph-color">Filial Marechal<br>C. Rondon - PR</p><span class="span-format">Fábrica</span>
      </div>
      <div class="col-md-6 index"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/img-campo.png" class="img-fluid pb-5"></div><img class="d-none d-md-block right-0 position-absolute mt-n5-1" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-way.png">
    </div>
  </div>
</section>
<section class="section-linha bg-dgrey py-5">
  <div class="container">
    <h2 class="py-5 text-md-left text-center text-white">Linha do tempo</h2>
    <div class="col-md-12">
      <div class="owl-carousel owl-theme" id="about-us">
        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card">2000</div>
            <div class="card-body">
              <h5 class="card-title text-white">Fundação da Alibra</h5>
              <p class="card-text text-white">Fundação da Alibra, com operações em Valinhos / SP. Foco no mercado B2C, atendendo atacadistas, distribuidores e, principalmente, montadores de cestas básicas.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card">2003</div>
            <div class="card-body">
              <h5 class="card-title text-white">Inauguração</h5>
              <p class="card-text text-white py-3">Inauguração da fábrica em Marechal Cândido Rondon/PR.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card">2004</div>
            <div class="card-body">
              <h5 class="card-title text-white">Transferência</h5>
              <p class="card-text text-white py-3">Fábrica de Valinhos/SP é transferida para Campinas/SP.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card">2005</div>
            <div class="card-body">
              <h5 class="card-title text-white">B2B</h5>
              <p class="card-text text-white py-3">Ínicio da atuação no mercado B2B (Ingredientes lácteos e não lácteos).</p>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="card border-primary mb-3">
            <div class="card-header title-card">2005</div>
            <div class="card-body">
              <h5 class="card-title text-white">B2B</h5>
              <p class="card-text text-white py-3">Ínicio da atuação no mercado B2B (Ingredientes lácteos e não lácteos).</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-between align-items-center">
        <div id="materiaDots" class="owl-dots col-md-6 my-4 my-md-0 d-flex"></div>
        <div id="materiaNav" class="owl-nav col-md-6 text-center text-md-right"></div>
      </div>
    </div>
  </div>
</section>
<section class="section-solucoes">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-4 col-2 d-none d-lg-block pl-0"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/formas.png" class="img-fluid"></div>
      <div class="col-md-4">
        <h2 class="title-solucoes text-md-left text-center">Soluções<br>Sob Medida</h2>
        <p class="text-md-left text-center paragraph-solucoes">Em todos segmentos em que atua, a Alibra desenvolve soluções<br>personalizadas de acordo com os perfis e as necessidades de<br>cada cliente.</p>
      </div>
      <div class="col-md-4 pr-md-0 d-flex justify-content-end"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/solucoes.png" class="img-fluid"></div>
    </div>
  </div>
</section>
<section class="section-premiacoes py-5 my-5">
  <div class="container">
    <h2 class="text-center py-3 title-premiacoes">Premiações</h2>
    <p class="text-center py-4 paragraph-premiacoes">Melhores do agronegócio</p>
    <div class="row justify-content-center py-3">
      <!-- Início Mobile -->
      <div class="col-md-3 d-md-none d-sm-block"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/premiacoes-1.png" class="img-fluid"></div>
      <div class="col-md-3 d-md-none d-sm-block"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/premiacoes-2.png" class="img-fluid"></div>
      <div class="col-md-3 d-md-none d-sm-block"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/premiacoes-3.png" class="img-fluid"></div><!-- Fim Mobile -->
      <div class="col-md-2 d-none d-md-block"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2014.png" class="img-fluid"></div>
      <div class="col-md-2 d-none d-md-block"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2015.png" class="img-fluid"></div>
      <div class="col-md-2 d-none d-md-block"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2016.png" class="img-fluid"></div>
      <div class="col-md-2 d-none d-md-block"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2017.png" class="img-fluid"></div>
      <div class="col-md-2 d-none d-md-block"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-2018.png" class="img-fluid"></div>
    </div>
    <hr class="hr-color">
    <p class="text-center py-4 paragraph-premiacoes">PME’s que mais crescem no Brasil</p>
    <div class="row">
      <div class="col-md-6"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pme-1.png" class="img-fluid"></div>
      <div class="col-md-6"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pme-2.png" class="img-fluid"></div>
    </div>
  </div>
</section>
<section class="section-exportacao full-center py-5 bg-alabastera">
  <div class="container bg-mapa h-100 py-5">
    <h2 class="text-center title-exportacao py-3">Exportação</h2>
    <p class="paragraph-exportacao text-center">A Alibra atua no mercado internacional, fornecendo seus produtos com a seriedade e a regularidade comercial exigida pelos órgãos de cada um dos países compradores.</p>
    <p class="paragraph-exportacao text-center">Inclusive, por meio da sua expertise e do alto investimento em pesquisa, inovação e desenvolvimento, a empresa conquistou as certificações que dão o direito a fornecer seus produtos também para países muçulmanos e para o mercado judaico.</p>
    <div class="row align-items-center">
      <div class="col-md-6 d-flex justify-content-center py-4"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exportacao-1.png"></div>
      <div class="col-md-6 d-flex justify-content-center"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exportacao-3.png"></div>
    </div>
  </div>
</section>
<section class="section-cia">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-8 px-md-5">
        <h2 class="title-cia">CIA - Centro de<br>Inovação e aplicação</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra orci vel urna maximus, nec varius nisl sagittis. Nam et nulla tellus. Donec vestibulum fermentum felis, dignissim molestie massa pellentesque at. Nulla convallis nunc pharetra sem volutpat semper. Vestibulum dignissim neque felis. Nunc id aliquam tellus. Integer ornare vitae nibh at luctus. Proin pulvinar orci tellus, at aliquam eros dignissim at. Cras venenatis laoreet risus in placerat.</p>
      </div>
      <div class="col-md-4 d-flex justify-content-md-end pr-md-0"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cia.png"></div>
    </div>
  </div>
</section>
<section class="section-planta bg-blue py-5">
  <div class="container">
    <h2 class="title-plant text-center text-white py-5">Planta Piloto UHT em Campinas - SP</h2>
    <div class="row align-items-center">
      <div class="col-md-6">
        <div class="owl-carousel owl-theme" id="plant">
          <div class="item"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lab.png" class="img-fluid"></div>
          <div class="item"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lab.png" class="img-fluid"></div>
          <div class="item"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lab.png" class="img-fluid"></div>
        </div>
      </div>
      <div class="col-md-6">
        <p class="paragraph-plant text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra orci vel urna maximus, nec varius nisl sagittis. Nam et nulla tellus. Donec vestibulum fermentum felis, dignissim molestie massa pellentesque at. Nulla convallis nunc pharetra sem volutpat semper. Vestibulum dignissim neque felis. Nunc id aliquam tellus. Integer ornare vitae nibh at luctus. Proin pulvinar orci tellus, at aliquam eros dignissim at. Cras venenatis laoreet risus in placerat.</p>
      </div>
    </div>
    <!-- <img class="d-none d-md-block right-0 position-absolute" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/way-green.png">
  </div> -->
  </div>
</section> <?php get_footer(); ?>