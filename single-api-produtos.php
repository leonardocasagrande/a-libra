<?php

function loadAjaxPosts($post_type, $taxonomy, $cat_name)
{

  //$next_page = $_POST['current_page'] + 1;

  $args = array(
    'post_type' => $post_type,
    'paged' => 1,
    'posts_per_page' => -1,
    'tax_query' =>  NULL
  );


  if (strlen($taxonomy) != 0) {

    $get_by_cat =  array(
      array(
        'taxonomy' => $taxonomy,
        'field'    => 'slug',
        'terms'    => $cat_name,
      )
    ); 

    $args['tax_query'] = $get_by_cat;
  }
 
  $query = new WP_Query($args);

  if ($query->have_posts()) :

    while ($query->have_posts()) : $query->the_post();
 

    $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

 
      $response = new stdClass; 

      $response->name = get_the_title();

      $response->image = $url;

      $response->grammage = [];  
 
      if (have_rows($post_type)) :

        while (have_rows($post_type)) : the_row();

       foreach (get_sub_field('gramatura_litro') as $key => $value){

          array_push($response->grammage, $value);
  
       }

        endwhile;
 
      else : 

      endif;

      foreach($response->grammage )

      $response = json_encode($response);

      echo $response;
 
    endwhile; 

  //  wp_send_json_success(ob_get_clean());

  else :

    wp_send_json_error('No more posts!');

  endif;
}

$post_type = $_POST['post_type'];

$taxonomy = $_POST['taxonomy'];

$cat_name = $_POST['cat_name'];

$posts_per_page = $_POST['posts_per_page'];

loadAjaxPosts($post_type, $taxonomy, $cat_name, $posts_per_page);

?>