<?php

/* Disable WordPress Admin Bar for all users but admins. */
show_admin_bar(false);

add_theme_support('category-thumbnails');

add_theme_support('post-thumbnails');


function replace_core_jquery_version()
{
  wp_deregister_script('jquery');
  // Change the URL if you want to load a local copy of jQuery from your own server.
  wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", array(), '3.4.1');
}
add_action('wp_enqueue_scripts', 'replace_core_jquery_version');



function create_post_type()
{

  /**
   * Produtos
   */
  register_post_type(
    'produtos',
    array(
      'supports' => array('title', 'thumbnail','editor'),
      'show_in_rest' => true,
      'labels' => array(
        'name' => __('Produtos'),
        'singular_name' => __('Produtos')
      ),
      'taxonomies' => array('categoria'),
      'public' => true,

    )
  );

  register_post_type(
    'api',
    array(
      'labels' => array(
        'name' => __('API'),
        'singular_name' => __('API')
      ),
      'public' => true,
      'has_archive' => false,
    )
  );
}

add_action('init', 'create_post_type');


//hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'create_topics_hierarchical_taxonomy', 0);

//create a custom taxonomy name it topics for your posts

function create_topics_hierarchical_taxonomy()
{

  // Add new taxonomy, make it hierarchical like categories
  //first do the translations part for GUI

  $labels = array(
    'name' => _x('Categoria', 'taxonomy general name'),
    'singular_name' => _x('Categorias', 'taxonomy singular name'),

    'menu_name' => __('Categorias'),
  );

  // Now register the taxonomy

  register_taxonomy('categoria', array('produto'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'show_in_rest'       => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'categoria'),
  ));
}



function cf_search_join($join)
{
  global $wpdb;
  if (is_search()) {
    $join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
  }
  return $join;
}
add_filter('posts_join', 'cf_search_join');

/**
 * Modify the search query with posts_where
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where($where)
{
  global $pagenow, $wpdb;
  if (is_search()) {
    $where = preg_replace(
      "/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
      "(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1)",
      $where
    );
  }
  return $where;
}
add_filter('posts_where', 'cf_search_where');

/**
 * Prevent duplicates
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct($where)
{
  global $wpdb;
  if (is_search()) {
    return "DISTINCT";
  }
  return $where;
}
add_filter('posts_distinct', 'cf_search_distinct');

/**
 * This is a modification of image_downsize() function in wp-includes/media.php
 * we will remove all the width and height references, therefore the img tag 
 * will not add width and height attributes to the image sent to the editor.
 * 
 * @param bool false No height and width references.
 * @param int $id Attachment ID for image.
 * @param array|string $size Optional, default is 'medium'. Size of image, either array or string.
 * @return bool|array False on failure, array on success.
 */
function myprefix_image_downsize( $value = false, $id, $size ) {
  if ( !wp_attachment_is_image($id) )
      return false;

  $img_url = wp_get_attachment_url($id);
  $is_intermediate = false;
  $img_url_basename = wp_basename($img_url);

  // try for a new style intermediate size
  if ( $intermediate = image_get_intermediate_size($id, $size) ) {
      $img_url = str_replace($img_url_basename, $intermediate['file'], $img_url);
      $is_intermediate = true;
  }
  elseif ( $size == 'thumbnail' ) {
      // Fall back to the old thumbnail
      if ( ($thumb_file = wp_get_attachment_thumb_file($id)) && $info = getimagesize($thumb_file) ) {
          $img_url = str_replace($img_url_basename, wp_basename($thumb_file), $img_url);
          $is_intermediate = true;
      }
  }

  // We have the actual image size, but might need to further constrain it if content_width is narrower
  if ( $img_url) {
      return array( $img_url, 0, 0, $is_intermediate );
  }
  return false;
}
/* Remove the height and width refernces from the image_downsize function.
 * We have added a new param, so the priority is 1, as always, and the new 
 * params are 3.
 */
add_filter( 'image_downsize', 'myprefix_image_downsize', 1, 3 );
?>